package org.cauldron.sigma.contentupload;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.pagectx.AbstractPageConfig;
import org.quantic.cauldron.pagectx.Messages;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.ToolData;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;

public class ContentUploadConfig extends AbstractPageConfig<Context> {

	@Override
	protected void execute(PageContext pagectx, Context context) {
		ToolData data;
		Messages messages;
		
		data = getTool("upload");
		data.style = "btn btn-primary";
		
		data = getTool("context.main");
		data.model = "context.main";
		data.instance(TYPES.FILE_UPLOAD, "file");
		data.instance(TYPES.TEXT_FIELD, "name");
		data.instance(TYPES.TEXT_FIELD, "text");
		data.instance("user_id").invisible = true;
		
		data = getTool("main");
		data.attributes.put("enctype", "multipart/form-data");
		
		messages = pagectx.getMessages("pt_BR");
		messages.put("upload", "Upload");
	}
	
}
