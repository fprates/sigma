package org.cauldron.sigma;

import java.util.HashMap;
import java.util.Map;

import org.cauldron.sigma.runtime.FormatData;
import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContext;
import org.quantic.cauldron.datamodel.TypeContextEntry;

public class Calculations {
	private Context context;
	public Map<String, STCalculation> calculations;
	public TypeContextEntry contentmodel, contentformat;
	
	public Calculations(Context context) {
		this.context = context;
		calculations = new HashMap<>();
		calculations.put("difal", new Difal());
		calculations.put("mi_adjusted_mva", new MIAdjustedMVA());
		calculations.put("adjusted_mva", new AdjustedMVA());
		calculations.put("source_mva", new SourceMVA());
		calculations.put("home", new Home());
	}
    
    public static final DataContextEntry getGridLayout(FormatData data) {
        DataContextEntry object;
        DataContextEntry objects = data.context.datactx().
        		instance("layout_format", "layout_format");
        
        for (String key : data.objects.getType().getParent().getItems()) {
            object = objects.instance();
            object.set("name", key);
            object.set("name_edit", key);
        }
        
        return objects;
    }
    
    public final FormatData getGridObjects(String[][] lines, int maxc) {
        String[] values = new String[maxc];
        for (int i = 0; i < maxc; i++)
            values[i] = new StringBuilder("COLUMN_").append(i).toString();
        return getGridObjects(lines, values);
    }
    
    private final FormatData getGridObjects(
    		String[][] lines, String[] mcolumns) {
        TypeContextEntry contentfmt;
        DataContextEntry object;
        FormatData data = new FormatData();
        TypeContext typectx = context.typectx();
        TypeContextEntry contentfmtln = typectx.define("content_format_line");
        
        for (String mcolumn : mcolumns)
        	contentfmtln.addst(mcolumn);
        
        contentfmt = typectx.define(contentfmtln, "content_format", null);
        contentfmt.array();
        
        data.objects = context.datactx().instance(contentfmt, "content_format");
        for (String[] columns : lines) {
            if (columns == null)
                continue;
            object = data.objects.instance();
            for (int i = 0; i < columns.length; i++)
                object.set(mcolumns[i],
                        (columns[i] == null)? null : columns[i].trim());
        }
        
        return data;
    }
	
	public final void updateTaxesView(DataContextEntry taxesview,
			DataContextEntry custtaxes, DataContextEntry sttaxes) {
		DataContextEntry taxview, ctitem, stitem;
		
		for (String ctkey : custtaxes.getItems())
			for (String stkey : sttaxes.getItems()) {
				ctitem = custtaxes.getItem(ctkey);
				stitem = sttaxes.getItem(stkey);
				if (!ctitem.getst("ncm").startsWith(stitem.getst("ncm")))
					continue;
				for (String ckey : calculations.keySet()) {
					taxview = taxesview.instance();
					taxview.set("calculation", ckey);
					taxview.copy(stitem);
					taxview.copy(ctitem);
					calculations.get(ckey).execute(taxview);
				}
			}
	}
}

class Difal implements STCalculation {
	
	@Override
	public final void execute(DataContextEntry taxview) {
		double icmsbaser = !taxview.getst("source").equals("target")?
			  taxview.getd("target_rate") - taxview.getd("interstate_mi_rate") :
				  taxview.getd("internal_rate");
		taxview.set("preview_icms_base_r", icmsbaser);
	}
}

class MIAdjustedMVA implements STCalculation {
	
	@Override
	public final void execute(DataContextEntry taxview) {
		double rate = !taxview.getst("source").equals("target")?
			taxview.getd("mi_adjusted_rate"): taxview.getd("original_rate");
		taxview.set("preview_rate", rate);
	}
}

class AdjustedMVA implements STCalculation {

	@Override
	public final void execute(DataContextEntry taxview) {
		double rate = !taxview.getst("source").equals("target")?
				taxview.getd("adjusted_rate"): taxview.getd("original_rate");
		taxview.set("preview_rate", rate);
	}
}

class SourceMVA implements STCalculation {

	@Override
	public final void execute(DataContextEntry taxview) {
		double rate = taxview.getd("original_rate");
		taxview.set("preview_rate", rate);
	}
}

class Home implements STCalculation {

	@Override
	public final void execute(DataContextEntry taxview) {
		double icmsbaser = !taxview.getst("source").equals("target")?
			  taxview.getd("target_rate") - taxview.getd("interstate_rate") :
					  taxview.getd("internal_rate");
		taxview.set("preview_icms_base_r", icmsbaser);
	}
}
