package org.cauldron.sigma.runtime;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.Session;

public class Test extends AbstractFunction {

	public Test() {
		super("test");
	}

	@Override
	protected void config(FunctionConfig config) { }

	@Override
	protected void execute(Session session) throws Exception {
		double rate, icmsbase;
		DataContextEntry taxview, ocusttax, ncusttax;
		CustomerTaxInclude ctinc;
		Context context = session.context();
		DataContext datactx = context.datactx();
		DataContextEntry taxesview = datactx.get("taxes_view");
		DataContextEntry custtaxes = datactx.get("cust_taxes");
		Date today = new Date();
		Map<String, CustomerTaxInclude> include = new LinkedHashMap<>();
		
		/*
		 * selecionamos quais impostos serão afetados. não podemos atualizá-los
		 * dentro do loop (exceção de concorrência)
		 */
		for (String tvkey : taxesview.getItems()) {
			taxview = taxesview.getItem(tvkey);
			if (!taxview.getbl("mark"))
				continue;
			
			for (String ctkey : custtaxes.getItems()) {
				ocusttax = custtaxes.getItem(ctkey);
				if (!isValidKey(taxview, ocusttax,
						"group", "ncm", "source", "target", "valid_from"))
					continue;
				include.put(ctkey, new CustomerTaxInclude(ocusttax, taxview));
			}
		}
		
		/*
		 * primeiro, atualizamos os impostos do cliente.
		 */
		for (String key : include.keySet()) {
			ctinc = include.get(key);
			ncusttax = custtaxes.instance();
			ncusttax.copy(ctinc.ocusttax);
			
			icmsbase = ctinc.taxview.getd("preview_icms_base_r");
			rate = ctinc.taxview.getd("preview_rate");
			
			ctinc.ocusttax.set("valid_to", today);
			ncusttax.set("valid_from", today);
			ncusttax.set("icms_base_r", icmsbase);
			ncusttax.set("rate", rate);
			custtaxes.remove(key);
		}

		/*
		 * em seguida, atualizamos a visao de impostos.
		 */
		for (String key : include.keySet()) {
			ctinc = include.get(key);
			icmsbase = ctinc.taxview.getd("preview_icms_base_r");
			rate = ctinc.taxview.getd("preview_rate");
			ctinc.taxview.set("icms_base_r", icmsbase);
			ctinc.taxview.set("rate", rate);
		}
	}
	
	private final boolean isValidKey(
			DataContextEntry taxview, DataContextEntry custtax, String... keys)
	{
		for (String key : keys)
			if (!taxview.get(key).equals(custtax.get(key)))
				return false;
		return true;
	}
	
}

class CustomerTaxInclude {
	public DataContextEntry ocusttax, taxview;
	
	public CustomerTaxInclude(
			DataContextEntry ocusttax, DataContextEntry taxview) {
		this.ocusttax = ocusttax;
		this.taxview = taxview;
	}
}
