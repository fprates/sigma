package org.cauldron.sigma.runtime;

import java.io.InputStream;

import org.cauldron.sigma.Calculations;

public interface FormatHandler {
    
    public abstract FormatData extract(
            Calculations context, InputStream is) throws Exception;
    
}