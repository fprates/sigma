package org.cauldron.sigma.runtime.formats;

import org.apache.poi.ss.usermodel.Cell;

public interface POICellHandler {
    
    public abstract Object get(Cell cell);
    
}
