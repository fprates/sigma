package org.cauldron.sigma;

import javax.servlet.annotation.WebServlet;

import org.cauldron.sigma.login.LoginConfig;
import org.cauldron.sigma.login.LoginSpec;
import org.cauldron.sigma.runtime.Connect;
import org.cauldron.sigma.systemreport.SystemReportConfig;
import org.cauldron.sigma.systemreport.SystemReportGet;
import org.cauldron.sigma.systemreport.SystemReportSpec;
import org.quantic.cauldron.application.Application;
import org.quantic.cauldron.application.web.AbstractCauldronServlet;
import org.quantic.cauldron.datamodel.TypeContext;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.pagectx.CauldronPage;
import org.quantic.cauldron.pagectx.CauldronPageType;

@WebServlet(name = "sigma-tests", urlPatterns = "/index.html")
public class Main extends AbstractCauldronServlet {
	private static final long serialVersionUID = 4099603880644075674L;
	
	public Main() {
		super("sigma");
	}
	
//		target.put("recover", "user_recovery_requested");
//		target.put("create", "user_create_requested");
//		
//		target = context.targetctx().instance();
//		target.put("material-upload", "materials");
//		target.put("taxes-upload", "customer-taxes");
//
//		page = pages.base("operation_select");
//		page.set(new OperationSelectSpec());
//		page.set(new OperationSelectConfig());
//		
//		page = pages.instance("ct_layout");
//		page.set(new CustomerTaxesLayoutSpec());
//		page.set(new CustomerTaxesLayoutConfig());
//		page.target("upload", "material-file");
//
//		page = pages.instance("ct_upload");
//		page.set(new CustomerTaxesUploadSpec());
//		page.set(new CustomerTaxesUploadConfig());
//		page.target("upload", "material-file");
//		
//		page = pages.instance("exhandler");
//		page.set(new ExHandlerSpec());
//		page.set(new ExHandlerConfig());
//		page.action("home", "page-actions", "home");
//
//		page = pages.instance("material_upload");
//		page.set(new MaterialUploadSpec());
//		page.set(new MaterialUploadConfig());
//		page.target("upload", "material-file");
//		
//		page = pages.instance("material_layout");
//		page.set(new MaterialLayoutSpec());
//		page.set(new MaterialLayoutConfig());
//		page.target("continue", "material-layout");
//
//		page = pages.instance("proposed_taxes");
//		page.set(new ProposedTaxesSpec());
//		page.set(new ProposedTaxesConfig());
//		page.target("upload", "material-file");
//		
//		context.setPageName("login");
    
	@Override
	protected final void init(Application application) {
		TypeContextEntry logon, type, programs_t;//, context_t;
		TypeContextEntry functions_t, parameters_t;//, cauldronprogs_t, pages_t;
		TypeContextEntry function_t, types_t;
		CauldronPage page = application.page("login");
		TypeContext typectx = application.getContext().typectx();
		
		typectx.define("session");
		
		logon = typectx.define("logon");
		logon.addst("user");
		logon.addst("secret");
		logon.addst("locale");
		
		type = typectx.define("connection_ok", "id");
		type.addl("id");
		type.add(logon, "user_request");
		type.autoGenKey();
		
		type = typectx.define("parameter");
		type.addst("name");
		
		types_t = typectx.define(type, "types", null);
		types_t.array();
		
		parameters_t = typectx.define("parameters");
		parameters_t.add(types_t, "inputs");
		parameters_t.add(types_t, "output");
		
		function_t = typectx.define("function");
		function_t.addst("name");
		function_t.add(parameters_t, "parameters");
		
		functions_t = typectx.define(function_t, "functions", null);
		functions_t.array();
//		cauldronprogs_t = typectx.define("cauldronprog", "cauldron_programs");
//		cauldronprogs_t.array();
//		pages_t = typectx.define("page", "pages");
//		pages_t.array();
		
		programs_t = typectx.define("programs");
		programs_t.add(functions_t, "functions");
//		programs_t.add(cauldronprogs_t, "programs");
//		programs_t.add(pages_t, "pages");
//		
//		context_t = typectx.define("context");
		
		type = typectx.define("system_report");
		type.add(programs_t, "programs");
//		type.add(context_t, "context");
		
		page.spec = new LoginSpec();
		page.config = new LoginConfig();
		page.type = CauldronPageType.BASE;
		page.output.add("logon");
		
		page = application.page("system_report");
		page.spec = new SystemReportSpec();
		page.config = new SystemReportConfig();
		page.type = CauldronPageType.PANEL;
		page.input.put("system_report", "sigma.system_report");
		
		application.add(()->new Connect());
		application.add(new SystemReportGet());
		
		application.target("page_call", "sigma.page_context");
		application.target("connect", "sigma.connection_ok");
		application.target("report", "sigma.display_document",
				(c)->c.signal.data.get("page").equals("system_report"));
		
		application.set(new MongoConnector("localhost", 27017, "tests"));
	}
}
