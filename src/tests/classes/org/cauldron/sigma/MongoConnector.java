package org.cauldron.sigma;

import java.util.HashSet;
import java.util.Set;

import org.quantic.cauldron.application.Connector;
import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.application.DBSession;
import org.quantic.cauldron.datamodel.TypeContextEntry;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoConnector implements Connector {
	private MongoClient dbclient;
	private MongoDatabase db;
	
	public MongoConnector(String host, int port, String db) {
		dbclient = new MongoClient(host, port);
		this.db = dbclient.getDatabase(db);
	}
	
	@Override
	public final void create(TypeContextEntry type) {
		db.createCollection(type.getFullName());
	}
	
	@Override
	public final Set<String> getDocuments() {
		Set<String> documents = new HashSet<>();
		for (String key : db.listCollectionNames())
			documents.add(key);
		return documents;
	}
	
	@Override
	public final DBSession instance(Context context) {
		return new MongoDBSession(context, db, dbclient.startSession());
	}
}
