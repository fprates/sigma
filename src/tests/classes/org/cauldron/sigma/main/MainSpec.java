package org.cauldron.sigma.main;

import org.quantic.cauldron.pagectx.AbstractPageSpec;

public class MainSpec extends AbstractPageSpec {

	@Override
	protected void execute() {
		dataform("content", "context.main");
		button("content", "upload");
	}
	
}

