package org.cauldron.sigma.main;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.output.TableFactory;
import org.quantic.cauldron.pagectx.ToolData;

public class TaxesViewTableFactory extends TableFactory {
    
    @Override
    protected final void generate(ContextEntry ctxentry,
            ToolData widget, String prefix) throws Exception {
    	super.generate(ctxentry, widget, prefix);
    	ToolData item, tbody = widget.items.get(widget.name.concat("_tbody"));
    	DataContextEntry data = ctxentry.context.datactx().get(widget.name);
    	
        for (String key : data.getItems()) {
        	item = tbody.items.get(key);
        	item.attributes.put("style", itemhl(data.getItem(key)));
        }
    }
    
    private final String itemhl(DataContextEntry item) {
    	for (String[] key : new String[][] {
    		{"icms_base_r", "preview_icms_base_r"},
    		{"rate", "preview_rate"} })
    		if (item.getd(key[0]) != item.getd(key[1]))
    			return "color:red";
    	return "color:green";
    }
}
