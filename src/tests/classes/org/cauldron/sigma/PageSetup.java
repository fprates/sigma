package org.cauldron.sigma;

import org.quantic.cauldron.pagectx.PageContext;

public interface PageSetup {

	public abstract void setup(PageContext page);
	
}

