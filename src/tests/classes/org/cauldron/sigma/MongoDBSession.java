package org.cauldron.sigma;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.application.DBSession;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.Query;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.datamodel.WhereClause;

import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;

public class MongoDBSession implements DBSession {
	private ClientSession session;
	private UpdateOptions upsert;
	private MongoDatabase db;
	private Context context;
	
	public MongoDBSession(
			Context context, MongoDatabase db, ClientSession session) {
		this.db = db;
		this.session = session;
		this.context = context;
		upsert = new UpdateOptions();
		upsert.upsert(true);
	}
	
	@Override
	public final void commit() {
		session.commitTransaction();
	}
	
	@SuppressWarnings("unchecked")
	private final <T> T convert(TypeContextEntry type, Document document) {
		TypeContextEntry child;
		Object value;
		String typekey;
		DataContextEntry data = context.datactx().
				instance(type, type.getName());
		
		typekey = type.getKey();
		for (String key : type.getItems()) {
			child = type.get(key);
			value = (key.equals(typekey))?
					document.get("_id") : document.get(key);
			data.set(key, child.getParent().getName().equals("any")?
					value : convert(type.get(key), (Document)value));
		}
		return (T)data;
	}
	
	@Override
	public final DataContextEntry get(TypeContextEntry type, Object key){
		MongoCollection<Document> collection =
				db.getCollection(type.getFullName());
		Document document = collection.find(Filters.eq("_id", key)).first();
		return (document == null)? null : convert(type, document);
	}
	
	@Override
	public final void insert(DataContextEntry data) {
		String collname = data.getType().getFullName();
		MongoCollection<Document> collection = db.getCollection(collname);
		collection.insertOne(session, instance(data));
	}
	
	@SuppressWarnings("unchecked")
	private final <T> T instance(DataContextEntry data) {
		Document document;
		TypeContextEntry type = data.getType();
		String key = type.getKey();
		Set<String> items = type.getItems();
				
		if (items.size() == 0)
			return data.get();

		document = (key == null)?
				new Document() : new Document("_id", data.get(key));
		for (String ikey : items)
			if (!ikey.equals(key))
				document.append(ikey, instance(data.getItem(ikey)));
		return (T)document;
	}
	
	@Override
	public final void rollback() {
		session.abortTransaction();
	}
	
	@Override
	public final void save(DataContextEntry data) {
		TypeContextEntry type = data.getType();
		String collname = type.getFullName();
		MongoCollection<Document> collection = db.getCollection(collname);
		
		collection.updateOne(session,
				Filters.eq("_id", data.get(type.getKey())),
				new Document("$set", instance(data)), upsert);
	}
	
	@Override
	public final void start() {
		session.startTransaction();
	}
	
	@Override
	public final void update(Query query) {
		String field;
		TypeContextEntry type = query.getType();
		String typekey = type.getKey();
		String typename = type.getName();
		MongoCollection<Document> collection = db.getCollection(typename);
		Map<String, Object> values = query.getValues();
		Bson[] changeset = new Bson[values.size()];
		Bson filter = null;
		int i = 0;
		
		for (String key : values.keySet())
			changeset[i++] = Updates.set(key, values.get(key));
		
		for (WhereClause clause : query.getWhere()) {
			field = (field = clause.getField()).equals(typekey)? "_id" : field;
			filter = Filters.eq(field, clause.getValue());
		}
		
		collection.updateOne(session, filter, Updates.combine(changeset));
	}
	
	@Override
	public final void update(DataContextEntry data) {
		TypeContextEntry type = data.getType();
		MongoCollection<Document> collection = db.getCollection(type.getName());
		Set<String> fields = type.getItems();
		List<Bson> changeset = new ArrayList<>();
		String key = type.getKey();
		
		for (String field : fields)
			if (!field.equals(key))
				changeset.add(Updates.set(field, data.get(field)));
		
		collection.updateOne(session,
				Filters.eq("_id", data.get(type.getKey())),
				Updates.combine(changeset));
	}
	
}